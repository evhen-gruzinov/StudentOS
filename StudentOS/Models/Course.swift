//
//  Created by Evhen Gruzinov on 23.11.2022.
//

import Foundation

struct Course {
    let id: Int
    var title: String
    var professor: String
}

enum EditModeTypes {case edit, create}
